//
// Created by Jason Power on 10/13/17.
//

#ifndef SDL_SCENEIDENTIFIERS_H
#define SDL_SCENEIDENTIFIERS_H


namespace SCENE {
    enum ID {
        NONE,
        TITLE,
        MAINMENU,
    };
}


#endif //SDL_SCENEIDENTIFIERS_H
