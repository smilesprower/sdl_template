//
// Created by Jason Power on 10/12/17.
//

#ifndef SDL_PACKETDATA_H
#define SDL_PACKETDATA_H

#include <vector>

enum class PacketType : int8_t {
    DISCONNECT = -1, CONNECT, HEARTBEAT, SNAPSHOT,
    UPDATE, MESSAGE, OUTOFBOUNDS
};

class PacketData {

public:
    PacketData();
//    ~PacketData() {};

    void append(const void *data, std::int8_t sizeInBytes);
    const void *getData() const;
    int8_t getDataSize() const;
    void stampPacket(const PacketType type);

    template<typename T>
    PacketData& operator <<(T data) {
        append(&data, sizeof(data));
        return *this;
    }

    template<typename T>
    PacketData& operator >> (T& data) {
        if (checkSize(sizeof(data))) {
            data = reinterpret_cast<const T&>(m_data[m_readPos]);
            m_readPos += sizeof(data);
        }
        return *this;
    }

private:
    std::vector<char> m_data;
    int8_t m_readPos;
    void clear();
    bool checkSize(int8_t size) const;

};
#endif //SDL_PACKETDATA_H
