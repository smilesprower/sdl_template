//
// Created by Jason Power on 9/30/17.
//
#include <chrono>
#include <thread>
#include <iostream>
#include "Application.h"
#include "ResourceManagers/TextureResourceManager.h"
#include "ResourceManagers/SoundResourceManager.h"
#include "ResourceManagers/MusicResourceManager.h"
#include "Network/Client.h"

Application::Application()
        : m_quit(false) {
}

Application::~Application() {
}

bool Application::initialize()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0){
        m_window = SDL_CreateWindow("SDL WINDOW ", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,400,300, SDL_WINDOW_INPUT_FOCUS);
        m_renderer = SDL_CreateRenderer(m_window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        SDL_SetRenderDrawColor(m_renderer, 50, 150, 215, 255);
        return true;
    }
    return false;
}

void Application::setUp() {

}

void Application::cleanUp() {
    SDL_DestroyWindow(m_window);
    SDL_DestroyRenderer(m_renderer);
    SDL_Quit();
}

void Application::loop() {



    // REMOVE
    m_quit = true;

    while (!m_quit) {

        while (SDL_PollEvent(&m_event)) {
            if (m_event.type == SDL_QUIT) {
                m_quit = true;
            }
        }

        // Update Scene
        SDL_RenderClear(m_renderer);					// Clear Renderer
        // Render Scene
        //SDL_RenderCopy(m_renderer, m_texture, nullptr, &rect);
        SDL_RenderPresent(m_renderer);					// Updates Screen

    }
}
//using Clock = std::chrono::steady_clock;
//using std::chrono::time_point;
//using std::chrono::duration_cast;
//using std::chrono::milliseconds;
//using namespace std::literals::chrono_literals;
//
//time_point<Clock> start = Clock::now();

//time_point<Clock> end = Clock::now();
//milliseconds diff = duration_cast<milliseconds>(end - start);
//std::cout << diff.count() << "ms" << std::endl;


//Client client;
//
//    TextureResourceManager textureMgr = TextureResourceManager(m_renderer);
//    textureMgr.loadResource("../Resources/Texture/test.png", Texture::MAINMENU);
//    m_texture = textureMgr.getResource(Texture::MAINMENU);
//    textureMgr.removeResource(Texture::MAINMENU);
//
//    SoundResourceManager soundResMgr = SoundResourceManager();
//    soundResMgr.loadResource("../Resources/SFX/test.wav", SFX::JUMP);
//    Mix_Chunk * m_sfx = soundResMgr.getResource(SFX::JUMP);
//    soundResMgr.removeResource(SFX::JUMP);
//
//    MusicResourceManager musicResMgr = MusicResourceManager();
//    musicResMgr.loadResource("../Resources/Music/test.wav", SFX::JUMP);
//    m_music = musicResMgr.getResource(MUSIC::JUMP);
//    musicResMgr.removeResource(MUSIC::JUMP);
//
//    SDL_Rect rect {0,0,166,195};


//    UDPpacket * m_packet;
//    m_packet = SDLNet_AllocPacket(std::numeric_limits<uint8_t >::max());
//
//
//    PacketData pData;
//    pData.stampPacket(PacketType::OUTOFBOUNDS);
//    pData << int32_t{-200} << uint32_t{300} << std::string{"hello"} << char{'2'} << bool{true};
//    std::cout << pData.getDataSize() << "\n";
//
//    if (pData.getDataSize() > 0) {
//        memcpy(m_packet->data, pData.getData(), m_packet->len = pData.getDataSize());
//    }
//
//    PacketData pData2;
//    pData2.append(m_packet->data, m_packet->len);
//
//    std::cout << pData2.getDataSize() << "\n";
//    int8_t abcde;
//    int32_t a;
//    uint32_t b;
//    std::string c;
//    char d;
//    bool e;
//
//    pData2 >> abcde >> a >> b >> c >> d >> e;
//    std::cout << (int)abcde << " " << b << " " << c << " " << d <<  " " << e;
//
