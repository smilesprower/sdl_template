//
// Created by Jason Power on 10/1/17.
//

#ifndef SDL_SCENE_H
#define SDL_SCENE_H

class Scene
{
public:

    Scene();
    virtual ~Scene();
    virtual void update() = 0;
    virtual void render() = 0;

    virtual void onEnter() = 0;
    virtual void onExit() = 0;

protected:
    void requestClearScene();
    void requestPushScene();
    void requestPopScene();

private:

};


#endif //SDL_SCENE_H
