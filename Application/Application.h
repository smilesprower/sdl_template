//
// Created by Jason Power on 9/30/17.
//

#ifndef SDL_APPLICATION_H
#define SDL_APPLICATION_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include "ResourceManagers/ResourceManager.h"

class Application {
public:
    Application();
    ~Application();

    bool initialize();
    void setUp();
    void cleanUp();
    void loop();

private:

    bool m_quit;
    SDL_Event m_event;
    SDL_Window * m_window;
    SDL_Renderer * m_renderer;
    SDL_Texture * m_texture;
    Mix_Chunk * m_sfx;
    Mix_Music * m_music;
};


#endif //SDL_APPLICATION_H
