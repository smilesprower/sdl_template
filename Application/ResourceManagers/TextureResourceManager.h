//
// Created by Jason Power on 10/1/17.
//

#ifndef SDL_TEXTURERESOURDEMANAGER_H
#define SDL_TEXTURERESOURDEMANAGER_H

#include "ResourceManager.h"

class TextureResourceManager : public ResourceManager<TextureResourceManager, SDL_Texture> {
public:
    TextureResourceManager(SDL_Renderer *renderer)
        : m_renderer(renderer) {
    }

    bool load(std::string resourcePath, SDL_Texture **texture) {
        SDL2::uniqueSurfacePtr surface(IMG_Load(resourcePath.c_str()));
        *texture = SDL_CreateTextureFromSurface(m_renderer, surface.get());
        return (*texture ? true : false);
    }
private:
    SDL_Renderer * m_renderer;
};

#endif //SDL_TEXTURERESOURDEMANAGER_H
