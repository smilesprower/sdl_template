//
// Created by Jason Power on 10/11/17.
//

#include "AudioPlayer.h"

AudioPlayer::AudioPlayer(uint32_t frequency, uint16_t format, uint8_t channels, uint16_t chunksize) {
    Mix_OpenAudio(frequency, format, channels, chunksize);
}