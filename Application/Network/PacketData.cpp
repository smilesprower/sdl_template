//
// Created by Jason Power on 10/12/17.
//

#include "PacketData.h"

PacketData::PacketData()
    : m_readPos(0) {
}

void PacketData::append(const void* data, int8_t sizeInBytes) {
    if (data && (sizeInBytes > 0)) {
        std::int8_t start = m_data.size();
        m_data.resize(start + sizeInBytes);
        std::memcpy(&m_data[start], data, sizeInBytes);
    }
}

void PacketData::stampPacket(const PacketType type) {
    *this << type;
}

const void* PacketData::getData() const {
    return !m_data.empty() ? &m_data[0] : nullptr;
}

int8_t PacketData::getDataSize() const {
    return m_data.size();
}

void PacketData::clear() {
    m_data.clear();
    m_readPos = 0;
}

bool PacketData::checkSize(int8_t size) const{
    return (m_readPos + size) <= m_data.size() ? true : false;
}