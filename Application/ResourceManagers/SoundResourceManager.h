//
// Created by Jason Power on 10/2/17.
//

#ifndef SDL_SOUNDRESOURCEMANAGER_H
#define SDL_SOUNDRESOURCEMANAGER_H

#include "ResourceManager.h"
#include <iostream>

class SoundResourceManager : public ResourceManager<SoundResourceManager, Mix_Chunk> {
public:
    SoundResourceManager() : ResourceManager() {
    }

    bool load(std::string resourcePath, Mix_Chunk **chunk) {
        *chunk = Mix_LoadWAV(resourcePath.c_str());
        return (*chunk ? true : false);
    }
};

#endif //SDL_SOUNDRESOURCEMANAGER_H
