#include <iostream>


#include "Application.h"
int main(int argc, char *argv[])
{

    Application app;

    app.initialize();
    app.setUp();
    app.loop();
    app.cleanUp();

    return 0;
}