//
// Created by Jason Power on 10/3/17.
//

#ifndef SDL_RESOURCEIDENTIFIERS_H
#define SDL_RESOURCEIDENTIFIERS_H


namespace TEXTURE {
    enum ID {
        MAINMENU,
        TITLE,
        GAME,
        PLAYER
    };
}

namespace SFX {
    enum ID {
        JUMP,
    };
}

namespace MUSIC {
    enum ID {
        JUMP,
    };
}

#endif //SDL_RESOURCEIDENTIFIERS_H
