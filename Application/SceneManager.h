//
// Created by Jason Power on 10/1/17.
//

#ifndef SDL_SCENEMANAGER_H
#define SDL_SCENEMANAGER_H

#include <SDL.h>
#include <map>
#include <vector>
#include <memory>
#include <functional>
#include "Scene.h"
#include "Scenes/SceneIdentifiers.h"
#include "NonCopyable.h"


class SceneManager : public Noncopyable {
public:
    SceneManager();
    ~SceneManager();

    void handleEvents();
    void update();											// Update the current scene
    void render();					                        // Render all scenes on the stack

    void pushScene(SCENE::ID scene);						// Push scene
    void popScene();
    void clearSceneStack();
    bool isSceneStackEmpty();								// Size of Scene Stack

    template <typename T>
    void createScene() {
        m_sceneFactory[SCENE::NONE] = [=]() {
            return std::make_unique<T>();
        };
    }

private:
    std::vector<SCENE::ID> m_currentScenes;
    std::vector<std::unique_ptr<Scene> > m_sceneStack;
    std::map<SCENE::ID, std::function<std::unique_ptr<Scene>()> > m_sceneFactory;

    void applyAwaitingChanges();
    std::unique_ptr<Scene> addScene(SCENE::ID scene);

};



#endif //SDL_SCENEMANAGER_H
