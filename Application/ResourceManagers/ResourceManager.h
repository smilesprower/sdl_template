
//
// Created by Jason Power on 10/1/17.
//

#ifndef SDL_RESOURCEMANAGER_H
#define SDL_RESOURCEMANAGER_H

#include <iostream>
#include <memory>
#include <unordered_map>
#include "UniqueSDL.h"
#include "ResourceIdentifiers.h"


template<typename T>
using ResourceContainer = std::unordered_map<int32_t, std::unique_ptr<T, SDL2::SDL_Deleter>>;

template<typename Class, typename Resource>
class ResourceManager {
public:
    //ResourceManager() {};

    virtual ~ResourceManager() {
        removeAllResources();
    };

    Resource * getResource(const int32_t resourceID) {
        const auto resource = m_resources.find(resourceID);
        return (resource != m_resources.end() ? resource->second.get() : nullptr);
    }

    bool loadResource(std::string resourcePath, int32_t resourceID) {
        Resource * resource;
        if(!load(resourcePath, &resource)) {
            return false;
        }
        m_resources.emplace(resourceID, std::move(resource));
        return true;
    }

    bool removeResource(int32_t resourceID) {
        const auto resource = m_resources.find(resourceID);
        if (resource == m_resources.end()) {
            return false;
        }
        m_resources.erase(resource);
        return true;
    }

private:
    bool load(std::string resourcePath, Resource **resource) {
        return static_cast<Class*>(this)->load(resourcePath, resource);
    }

    void removeAllResources() {
        while(m_resources.begin() != m_resources.end()) {
            m_resources.erase(m_resources.begin());
        }
    }

    std::unordered_map<int32_t, std::unique_ptr<Resource, SDL2::SDL_Deleter>> m_resources;
};

#endif //SDL_RESOURCEMANAGER_H
