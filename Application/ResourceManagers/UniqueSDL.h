//
// Created by Jason Power on 10/4/17.
//

#ifndef SDL_UNIQUESDL_H
#define SDL_UNIQUESDL_H

namespace SDL2 {
    struct SDL_Deleter {
        void operator()(SDL_Surface*  ptr) {
            if (ptr) SDL_FreeSurface(ptr);
        }
        void operator()(SDL_Texture*  ptr) {
            if (ptr) SDL_DestroyTexture(ptr);
        }
        void operator()(Mix_Chunk*  ptr) {
            if (ptr) Mix_FreeChunk(ptr);
        }
        void operator()(Mix_Music*  ptr) {
            if (ptr) Mix_FreeMusic(ptr);
        }
    };
// Unique Pointers
    using uniqueSurfacePtr  = std::unique_ptr<SDL_Surface,  SDL_Deleter>;
    using uniqueTexturePtr  = std::unique_ptr<SDL_Texture,  SDL_Deleter>;
}
#endif //SDL_UNIQUESDL_H
