//
// Created by Jason Power on 10/1/17.
//

#include "SceneManager.h"
SceneManager::SceneManager() {

}

SceneManager::~SceneManager() {

}

void SceneManager::handleEvents() {}

void SceneManager::update() {}

void SceneManager::render() {}

void SceneManager::pushScene(SCENE::ID scene) {}

void SceneManager::popScene() {}

void SceneManager::clearSceneStack() {}

std::unique_ptr<Scene> SceneManager::addScene(SCENE::ID scene) {

}

void SceneManager::applyAwaitingChanges() {}