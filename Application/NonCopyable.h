//
// Created by Jason Power on 10/1/17.
//

#ifndef SDL_NONCOPYABLE_H
#define SDL_NONCOPYABLE_H

class Noncopyable {
public:
    Noncopyable() = default;
    ~Noncopyable() = default;

private:
    Noncopyable(const Noncopyable&) = delete;
    Noncopyable& operator=(const Noncopyable&) = delete;
};

#endif //SDL_NONCOPYABLE_H
