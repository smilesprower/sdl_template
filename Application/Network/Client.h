//
// Created by Jason Power on 10/11/17.
//

#ifndef SDL_CLIENT_H
#define SDL_CLIENT_H

#include <iostream>
#include <SDL_net.h>
#include "PacketData.h"

class Client {
public:
    Client(){};
    ~Client(){};

    UDPsocket m_socket;
    UDPpacket * m_packet;
    IPaddress m_serverIP;

    const std::string HOST = "127.0.0.1";
    const Uint16 SERVER_PORT = 5228;
    const int TIMEOUT = 5000;
};


#endif //SDL_CLIENT_H
