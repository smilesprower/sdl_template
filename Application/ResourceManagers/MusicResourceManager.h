//
// Created by Jason Power on 10/10/17.
//

#ifndef SDL_MUSICRESOURCEMANAGER_H
#define SDL_MUSICRESOURCEMANAGER_H

#include "ResourceManager.h"

class MusicResourceManager : public ResourceManager<MusicResourceManager, Mix_Music> {
public:
    MusicResourceManager() : ResourceManager() {
    }

    bool load(std::string resourcePath, Mix_Music **music) {
        *music = Mix_LoadMUS(resourcePath.c_str());
        return (*music ? true : false);
    }
};

#endif //SDL_MUSICRESOURCEMANAGER_H
